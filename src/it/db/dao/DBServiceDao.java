package it.db.dao;

import it.db.filter.FiltroElemento;
import it.db.mapper.DBServiceMapper;
import it.db.model.Elemento;
import it.db.model.Sottoelemento;
import it.db.mybatis.SqlMapFactory;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class DBServiceDao {

	private static Logger _log = Logger.getLogger(DBServiceDao.class);

	private DBServiceMapper getSqlExecutor() {
		return SqlMapFactory.instance().getMapper(DBServiceMapper.class);
	}

	private DBServiceMapper getSqlExecManaged() {
		return SqlMapFactory.instance().getMapperManaged(DBServiceMapper.class);
	}

	public String welcomeTest() {
		SqlMapFactory.instance().openSession();
		String result = null;

		try {
			result = getSqlExecutor().welcomeTest();
		} catch (Exception e) {
			e.printStackTrace();
			_log.error(e, e);
		} finally {
			SqlMapFactory.instance().closeSession();
		}

		return result;
	}

	public List<Elemento> getElementi() {

		SqlMapFactory.instance().openSession();
		List<Elemento> elementi = new ArrayList<>();

		try {
			elementi = getSqlExecutor().getElementi();

		} catch (Exception e) {
			e.printStackTrace();
			_log.error(e, e);

		} finally {
			SqlMapFactory.instance().closeSession();
		}

		return elementi;
	}

	public boolean addElemento(Elemento elemento) {
		boolean added = false;
		SqlMapFactory.instance().openSession();
		try {

			getSqlExecutor().addElemento(elemento);
			SqlMapFactory.instance().commitSession();

			Elemento _elemento = new Elemento();
			_elemento = getSqlExecutor().getElemento(elemento.getId());
			if (_elemento != null)
				added = true;

		} catch (Exception e) {
			e.printStackTrace();
			_log.error(e, e);

		} finally {
			SqlMapFactory.instance().closeSession();
		}
		// System.out.println(elemento.getId().toString());
		return added;
	}

	public Elemento getElemento(Long id) {
		SqlMapFactory.instance().openSession();
		Elemento elemento = null;
		try {
			elemento = getSqlExecutor().getElemento(id);

		} catch (Exception e) {
			e.printStackTrace();
			_log.error(e, e);

		} finally {
			SqlMapFactory.instance().closeSession();
		}
		return elemento;
	}

	public boolean deleteElemento(Long id) {
		boolean deleted = false;
		SqlMapFactory.instance().openSession();
		try {
			getSqlExecutor().deleteElemento(id);
			SqlMapFactory.instance().commitSession();

			Elemento elemento = null;
			elemento = getSqlExecutor().getElemento(id);
			List<Sottoelemento> list = null;
			list = getSqlExecutor().getSottoelementiById(id);

			if (elemento == null && (list == null || list.isEmpty()))
				deleted = true;

		} catch (Exception e) {
			e.printStackTrace();
			_log.error(e, e);

		} finally {
			SqlMapFactory.instance().closeSession();
		}
		return deleted;

	}

	public boolean updateElemento(Elemento elemento) {
		boolean updated = false;
		SqlMapFactory.instance().openSession();

		System.out.println(elemento.getId().toString());
		System.out.println(elemento.getNome().toString());
		System.out.println(elemento.getDescrizione().toString());
		System.out.println(elemento.getData().toString());

		try {
			getSqlExecutor().updateElemento(elemento);
			SqlMapFactory.instance().commitSession();
			System.out.println(elemento.getId().toString());
			Elemento _elemento = new Elemento();
			_elemento = getSqlExecutor().getElemento(elemento.getId());

			System.out.println("_" + _elemento.getId().toString());
			System.out.println("_" + _elemento.getNome().toString());
			System.out.println("_" + _elemento.getDescrizione().toString());
			System.out.println("_" + _elemento.getData().toString());

			if (_elemento.equals(elemento))
				updated = true;

		} catch (Exception e) {
			e.printStackTrace();
			_log.error(e, e);

		} finally {
			SqlMapFactory.instance().closeSession();
		}
		return updated;
	}

	public List<Sottoelemento> getSottoelementiById(Long id) {
		SqlMapFactory.instance().openSession();
		List<Sottoelemento> sottoelementi = new ArrayList<>();

		try {
			sottoelementi = getSqlExecutor().getSottoelementiById(id);

		} catch (Exception e) {
			e.printStackTrace();
			_log.error(e, e);

		} finally {
			SqlMapFactory.instance().closeSession();
		}

		return sottoelementi;
	}

	public void addSottoelemento(Sottoelemento sottoelemento) {
		SqlMapFactory.instance().openSession();
		try {
			getSqlExecutor().addSottoelemento(sottoelemento);
			SqlMapFactory.instance().commitSession();

		} catch (Exception e) {
			e.printStackTrace();
			_log.error(e, e);

		} finally {
			SqlMapFactory.instance().closeSession();
		}
		// System.out.println(elemento.getId().toString());
	}

	public List<Elemento> getElementiFilter(FiltroElemento filtroElemento) {
		SqlMapFactory.instance().openSession();
		List<Elemento> elementi = new ArrayList<>();

		if (filtroElemento.getData() != null) {

			Timestamp ts = new Timestamp(filtroElemento.getData().getTime());
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			filtroElemento.set_dateString(formatter.format(ts));
		}

		try {
			elementi = getSqlExecutor().getElementiFilter(filtroElemento);

			System.out.println("dao_" + filtroElemento.getId());
			System.out.println("dao_" + filtroElemento.getNome());
			System.out.println("dao_" + filtroElemento.getDescrizione());
			System.out.println("dao_" + filtroElemento.getData());
			System.out.println("dao_" + filtroElemento.get_dateString());

		} catch (Exception e) {
			e.printStackTrace();
			_log.error(e, e);

		} finally {
			SqlMapFactory.instance().closeSession();
		}
		return elementi;
	}
}
