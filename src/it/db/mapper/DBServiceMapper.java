package it.db.mapper;

import it.db.filter.FiltroElemento;
import it.db.model.Elemento;
import it.db.model.Sottoelemento;

import java.util.List;

public interface DBServiceMapper {

	public List<Elemento> getElementi();

	public Elemento getElemento(Long id);

	public String welcomeTest();

	public void addElemento(Elemento elemento);

	public void deleteElemento(Long id);

	public void updateElemento(Elemento elemento);

	public List<Sottoelemento> getSottoelementiById(Long id);

	public void addSottoelemento(Sottoelemento sottoelemento);

	public List<Elemento> getElementiFilter(FiltroElemento filtroElemento);
}
