package it.db.swagger;

import io.swagger.jaxrs.listing.ApiListingResource;

import javax.ws.rs.Path;

@Path("/DBRService/api/DBService.{type:json|yaml}")
public class DBServiceApiListingResource extends ApiListingResource {

}