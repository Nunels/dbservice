package it.db.swagger;

import io.swagger.jaxrs.config.BeanConfig;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class Bootstrap extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String SWAGGER_REST_SERVICE_VERSION = "1.0.2";
	private static final String SWAGGER_REST_SERVICE_TITLE = "API REST DB Service";
	private static final String SWAGGER_REST_SERVICE_HOST = "localhost:9080";
	private static final String SWAGGER_REST_SERVICE_BASE_PATH = "/DBService/services";
	private static final String SWAGGER_REST_SERVICE_RESOURCE_PACK = "it.db.service";

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setVersion(SWAGGER_REST_SERVICE_VERSION);
		beanConfig.setTitle(SWAGGER_REST_SERVICE_TITLE);
		// beanConfig.setHost(SWAGGER_REST_SERVICE_HOST);
		beanConfig.setBasePath(SWAGGER_REST_SERVICE_BASE_PATH);
		beanConfig.setResourcePackage(SWAGGER_REST_SERVICE_RESOURCE_PACK);
		// System.out.println("ctxp"+ getServletContext().getContextPath());
		// System.out.println("ctxp"+ getServletContext().getServerInfo());

		// parametri per distringuere il contesto
		beanConfig.setConfigId("DBS");
		beanConfig.setContextId("DBS");
		beanConfig.setScannerId("DBS");

		beanConfig.setScan(true);
	}
}