package it.db.service;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.db.ejb.DBServiceEJB;
import it.db.filter.FiltroElemento;
import it.db.model.Elemento;
import it.db.model.ElementoDto;
import it.db.model.Sottoelemento;
import it.db.model.SottoelementoDto;
import it.db.validation.ValidationUtil;
import it.sogei.data.DataUtilFactory;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

@Path("/DBRService")
@Api(value = "DBRService")
public class DBService {

	private @EJB DBServiceEJB ejb = new DBServiceEJB();

	private final String mediaType = MediaType.APPLICATION_JSON;

	public DBService() {
	}

	@POST
	@Path("welcomeTest")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Tutto OK"),
			@ApiResponse(code = 500, message = "Errore interno.") })
	public Response welcomeTest(
			@Context HttpServletRequest httpServletRequest,
			@Context SecurityContext context,
			@ApiParam(value = "Tipo di contenuto", required = true) @HeaderParam(HttpHeaders.CONTENT_TYPE) String type,
			@HeaderParam(HttpHeaders.ACCEPT_LANGUAGE) String language) {

		Response response;

		if (!validateContentType(type)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		String result = ejb.welcomeTest();

		if (result != null) {

			response = Response.status(Status.OK).build();

		} else {
			response = Response.status(Status.BAD_REQUEST).build();
		}

		return response;
	}

	@POST
	@Path("/deleteElemento")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Tutto OK", response = Elemento.class),
			@ApiResponse(code = 406, message = "Dati input errati."),
			@ApiResponse(code = 500, message = "Errore interno.") })
	public Response deleteElemento(
			@Context HttpServletRequest httpServletRequest,
			@Context SecurityContext context,
			@ApiParam(value = "Tipo di contenuto", required = true) @HeaderParam(HttpHeaders.CONTENT_TYPE) String type,
			@HeaderParam(HttpHeaders.ACCEPT_LANGUAGE) String language,
			@ApiParam(value = "ID elemento", required = true) Elemento elemento) {

		if (!validateContentType(type)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		boolean deleted = ejb.deleteElemento(elemento.getId());

		if (!deleted) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		return Response.status(Status.OK).build();
	}

	@POST
	@Path("/getElemento")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Tutto OK"),
			@ApiResponse(code = 406, message = "Dati input errati."),
			@ApiResponse(code = 500, message = "Errore interno.") })
	public Response getElemento(
			@Context HttpServletRequest httpServletRequest,
			@Context SecurityContext context,
			@ApiParam(value = "Tipo di contenuto", required = true) @HeaderParam(HttpHeaders.CONTENT_TYPE) String type,
			@HeaderParam(HttpHeaders.ACCEPT_LANGUAGE) String language,
			@ApiParam(value = "elemento", required = true) Elemento elemento)
			throws UnsupportedEncodingException {

		Response response = null;

		if (!validateContentType(type) || !ValidationUtil.validate(elemento)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		elemento = ejb.getElemento(elemento.getId());

		if (elemento != null) {

			response = Response
					.status(Status.OK)
					.type(type)
					.entity(new String(DataUtilFactory
							.trasformObjectToJsonString(elemento), "UTF-8"))
					.build();
		} else {

			response = Response.status(Status.BAD_REQUEST).build();
		}

		return response;
	}

	@POST
	@Path("/getElementi")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Tutto OK", response = ElementoDto.class),
			@ApiResponse(code = 406, message = "Dati input errati."),
			@ApiResponse(code = 500, message = "Errore interno.") })
	public Response getElementi(
			@Context HttpServletRequest httpServletRequest,
			@Context SecurityContext context,
			@ApiParam(value = "Tipo di contenuto", required = true) @HeaderParam(HttpHeaders.CONTENT_TYPE) String type,
			@HeaderParam(HttpHeaders.ACCEPT_LANGUAGE) String language)
			throws UnsupportedEncodingException {

		ElementoDto elementoDto = new ElementoDto();
		List<Elemento> elementi = ejb.getElementi();

		if (!validateContentType(type)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		Response response;
		if (!(elementi == null) && !elementi.isEmpty()) {

			elementoDto.setListElemento(elementi);

			response = Response
					.status(Status.OK)
					.type(type)
					.entity(new String(DataUtilFactory
							.trasformObjectToJsonString(elementoDto), "UTF-8"))
					.build();

		} else {

			// response = generateResponse(Response.Status.BAD_REQUEST, type,
			// null, null);
			response = Response.status(Status.BAD_REQUEST).build();
		}

		return response;
	}

	@POST
	@Path("/addElemento")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Tutto OK"),
			@ApiResponse(code = 406, message = "Dati input errati."),
			@ApiResponse(code = 500, message = "Errore interno.") })
	public Response addlemento(
			@Context HttpServletRequest httpServletRequest,
			@Context SecurityContext context,
			@ApiParam(value = "Tipo di contenuto", required = true) @HeaderParam(HttpHeaders.CONTENT_TYPE) String type,
			@HeaderParam(HttpHeaders.ACCEPT_LANGUAGE) String language,
			@ApiParam(value = "Elemento da aggiungere", required = true) Elemento elemento)
			throws UnsupportedEncodingException {

		if (!validateContentType(type) || !ValidationUtil.validate(elemento)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		if (!(elemento == null)) {
			boolean added = ejb.addElemento(elemento);

			if (added)
				return Response.status(Response.Status.OK).type(type).build();

		}

		return Response.status(Status.BAD_REQUEST).build();

	}

	@POST
	@Path("/getSottoelementiById")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Tutto OK", response = ElementoDto.class),
			@ApiResponse(code = 406, message = "Dati input errati."),
			@ApiResponse(code = 500, message = "Errore interno."),
			@ApiResponse(code = 204, message = "Vuoto") })
	public Response getSottoelementiById(
			@Context HttpServletRequest httpServletRequest,
			@Context SecurityContext context,
			@ApiParam(value = "Tipo di contenuto", required = true) @HeaderParam(HttpHeaders.CONTENT_TYPE) String type,
			@HeaderParam(HttpHeaders.ACCEPT_LANGUAGE) String language,
			@ApiParam(value = "Elemento per sottoelementi", required = true) Elemento elemento)
			throws UnsupportedEncodingException {

		SottoelementoDto sottoelementoDto = new SottoelementoDto();
		List<Sottoelemento> sottoelementi = ejb.getSottoelementiById(elemento
				.getId());

		if (!validateContentType(type) || !ValidationUtil.validate(elemento)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		Response response;
		if (!(sottoelementi == null) && !sottoelementi.isEmpty()) {

			sottoelementoDto.setListSottoelemento(sottoelementi);

			response = Response
					.status(Status.OK)
					.type(type)
					.entity(new String(DataUtilFactory
							.trasformObjectToJsonString(sottoelementoDto),
							"UTF-8")).build();

		} else {

			// response = generateResponse(Response.Status.BAD_REQUEST, type,
			// null, null);
			response = Response.status(Status.BAD_REQUEST).build();
		}

		return response;
	}

	@POST
	@Path("/updateElemento")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Tutto OK"),
			@ApiResponse(code = 406, message = "Dati input errati."),
			@ApiResponse(code = 500, message = "Errore interno.") })
	public Response updateElemento(
			@Context HttpServletRequest httpServletRequest,
			@Context SecurityContext context,
			@ApiParam(value = "Tipo di contenuto", required = true) @HeaderParam(HttpHeaders.CONTENT_TYPE) String type,
			@HeaderParam(HttpHeaders.ACCEPT_LANGUAGE) String language,
			@ApiParam(value = "Elemento da aggiornare", required = true) Elemento elemento)
			throws UnsupportedEncodingException {

		if (!validateContentType(type) || !ValidationUtil.validate(elemento)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		if (elemento != null) {
			boolean updated = ejb.updateElemento(elemento);

			if (updated) {
				return Response.status(Response.Status.OK).build();
			}

		}

		// response = generateResponse(Response.Status.BAD_REQUEST, type,
		// null, null);
		return Response.status(Status.BAD_REQUEST).build();

	}

	@POST
	@Path("/getElementiFilter")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Tutto OK"),
			@ApiResponse(code = 406, message = "Dati input errati."),
			@ApiResponse(code = 500, message = "Errore interno.") })
	public Response getElementiFilter(
			@Context HttpServletRequest httpServletRequest,
			@Context SecurityContext context,
			@ApiParam(value = "Tipo di contenuto", required = true) @HeaderParam(HttpHeaders.CONTENT_TYPE) String type,
			@HeaderParam(HttpHeaders.ACCEPT_LANGUAGE) String language,
			@ApiParam(value = "filtro per elemento", required = true) FiltroElemento filtroElemento)
			throws UnsupportedEncodingException {

		Response response = null;

		if (!validateContentType(type)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		List<Elemento> elementi = null;
		ElementoDto elementoDto = new ElementoDto();
		FiltroElemento newFiltroElemento = ValidationUtil
				.adjustFiltroElemento(filtroElemento);
		elementi = ejb.getElementiFilter(filtroElemento);

		System.out.println("dbs_" + newFiltroElemento.getId());
		System.out.println("dbs_" + newFiltroElemento.getNome());
		System.out.println("dbs_" + newFiltroElemento.getDescrizione());
		System.out.println("dbs_" + newFiltroElemento.getData());

		if (elementi != null) {
			elementoDto.setListElemento(elementi);
			response = Response
					.status(Status.OK)
					.type(type)
					.entity(new String(DataUtilFactory
							.trasformObjectToJsonString(elementoDto), "UTF-8"))
					.build();
		} else {

			response = Response.status(Status.BAD_REQUEST).build();
		}

		return response;
	}

	public boolean validateContentType(String type) {
		if (type == null
				|| (!MediaType.APPLICATION_XML.equalsIgnoreCase(type) && !MediaType.APPLICATION_JSON
						.equalsIgnoreCase(type))) {
			return false;
		}
		return true;
	}
}
