package it.db.controller;

import it.db.dao.DBServiceDao;
import it.db.filter.FiltroElemento;
import it.db.model.Elemento;
import it.db.model.Sottoelemento;

import java.util.ArrayList;
import java.util.List;

public class DBServiceController {

	DBServiceDao dao = new DBServiceDao();

	public String welcomeTest() {
		String result = null;
		try {

			result = dao.welcomeTest();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<Elemento> getElementi() {

		List<Elemento> elementi = new ArrayList<>();

		try {
			elementi = dao.getElementi();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return elementi;
	}

	public Elemento getElemento(Long id) {

		Elemento elemento = null;

		try {
			elemento = dao.getElemento(id);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return elemento;
	}

	public boolean addElemento(Elemento elemento) {
		boolean added = false;
		try {
			added = dao.addElemento(elemento);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return added;
	}

	public boolean deleteElemento(Long id) {
		boolean deleted = false;
		try {
			deleted = dao.deleteElemento(id);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return deleted;
	}

	public boolean updateElemento(Elemento elemento) {
		boolean updated = false;
		try {
			updated = dao.updateElemento(elemento);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return updated;
	}

	public List<Sottoelemento> getSottoElementiById(Long id) {

		List<Sottoelemento> sottoelementi = new ArrayList<>();

		try {
			sottoelementi = dao.getSottoelementiById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sottoelementi;
	}

	public List<Elemento> getElementiFilter(FiltroElemento filtroElemento) {
		List<Elemento> elementi = new ArrayList<>();

		try {
			elementi = dao.getElementiFilter(filtroElemento);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return elementi;
	}

}
