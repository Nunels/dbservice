package it.db.mybatis;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.session.SqlSessionManager;
import org.apache.ibatis.session.TransactionIsolationLevel;

public class SqlMapFactory {
	private String resource = "it/db/mybatis/mybatis-config.xml";

	private static ThreadLocal<SqlMapFactory> THREAD_LOCAL = new ThreadLocal<SqlMapFactory>() {
		@Override
		protected SqlMapFactory initialValue() {
			return new SqlMapFactory();
		}
	};

	private SqlSessionManager sqlSessionManager;
	private SqlSessionFactory sqlSessionFactory;
	private SqlSession sqlSession;

	public static SqlMapFactory instance() {
		return THREAD_LOCAL.get();
	}

	private SqlSession session = null;

	public SqlSession getSession() {
		try {
			Reader reader = Resources.getResourceAsReader(resource);
			SqlSessionFactory sqlMapper = new SqlSessionFactoryBuilder()
					.build(reader);
			session = sqlMapper.openSession();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return session;
	}

	private SqlMapFactory() {
	}

	private SqlSessionManager getSqlSessionManager() {
		if (instance().sqlSessionManager == null) {
			instance().sqlSessionManager = SqlSessionManager
					.newInstance(instance().getSqlSessionFactory());
		}

		return instance().sqlSessionManager;
	}

	private SqlSessionFactory getSqlSessionFactory() {

		if (instance().sqlSessionFactory == null) {
			try {
				Reader reader = Resources.getResourceAsReader(resource);
				SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
				instance().sqlSessionFactory = builder.build(reader);
			} catch (Exception e) {
				throw new RuntimeException(
						"Errore nell'inizializzazione della classe SqlSessionFactory. Causa: "
								+ e);
			}
		}

		return instance().sqlSessionFactory;
	}

	public SqlSession openSession() {
		if (instance().sqlSession == null) {
			instance().sqlSession = instance().getSqlSessionFactory()
					.openSession();
		}
		return instance().sqlSession;
	}

	public SqlSession openSessionNoAutoCommit() {
		if (instance().sqlSession == null) {
			instance().sqlSession = instance().getSqlSessionFactory()
					.openSession(false);
		}
		return instance().sqlSession;
	}

	public void commitSession() {
		if (instance().sqlSession != null) {
			instance().sqlSession.commit(true);
		}
	}

	public void closeSession() {
		if (instance().sqlSession != null) {
			instance().sqlSession.close();
			instance().sqlSession = null;
		}
	}

	public void rollbackSession() {
		if (instance().sqlSession != null) {
			instance().sqlSession.rollback();
		}

	}

	public <T> T getMapper(Class<T> type) {
		if (instance().sqlSession != null) {
			return (T) instance().sqlSession.getMapper(type);
		}
		return null;
	}

	public void openSessionLocalManaged() {
		if (instance().sqlSessionManager == null) {
			instance().getSqlSessionManager().startManagedSession(
					ExecutorType.SIMPLE,
					TransactionIsolationLevel.READ_COMMITTED);
		}
	}

	public void commitSessionLocalManaged() {
		if (instance().sqlSessionManager != null) {
			instance().sqlSessionManager.commit(true);
		}
	}

	public void closeSessionLocalManaged() {
		if (instance().sqlSessionManager != null) {
			instance().sqlSessionManager.close();
			instance().sqlSessionManager = null;
		}
	}

	public void rollbackSessionLocalManaged() {
		if (instance().sqlSessionManager != null) {
			instance().sqlSessionManager.rollback();
		}

	}

	public <T> T getMapperManaged(Class<T> type) {
		if (instance().sqlSessionManager != null) {
			return (T) instance().sqlSessionManager.getMapper(type);
		}
		return null;
	}

}
