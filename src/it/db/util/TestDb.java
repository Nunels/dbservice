package it.db.util;

import it.db.dao.DBServiceDao;
import it.db.filter.FiltroElemento;
import it.db.model.Elemento;

import java.util.ArrayList;
import java.util.List;

public class TestDb {

	public static void main(String[] args) {
		testQuery();
	}

	public static void testQuery() {
		FiltroElemento fe = new FiltroElemento();
		DBServiceDao dao = new DBServiceDao();
		List<Elemento> elementi = new ArrayList<>();

		String s = "ann";
		fe.setNome("%" + s + "%");
		elementi = dao.getElementiFilter(fe);

		if (elementi != null) {
			for (Elemento elemento : elementi) {

				System.out.println(elemento.getId());
				System.out.println(elemento.getNome());
				System.out.println(elemento.getData());
				System.out.println(elemento.getDescrizione());
			}
		} else {
			System.out.println("vuota");
		}

	}
}
