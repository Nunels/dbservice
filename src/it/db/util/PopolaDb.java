package it.db.util;

import it.db.dao.DBServiceDao;
import it.db.model.Elemento;
import it.db.model.Sottoelemento;

import java.util.Date;

import com.github.javafaker.Faker;

public class PopolaDb {

	public static void main(String[] args) {

		for (int i = 0; i < 50; i++) {
			popola();
		}

	}

	public static void popolaElementi() {
		Faker faker = new Faker();
		String nome = faker.name().fullName();
		String descrizione = faker.address().streetAddress();
		Date data = (Date) faker.date().birthday();
		Elemento elemento = new Elemento(nome, data, descrizione);

		DBServiceDao dao = new DBServiceDao();
		dao.addElemento(elemento);
	}

	public static void update() {
		DBServiceDao dao = new DBServiceDao();
		Elemento elemento = new Elemento("ciao", new Date(), "descr");
		elemento.setId(146L);
		dao.updateElemento(elemento);
	}

	public static void popolaSottoElementi() {
		Faker faker = new Faker();

		String nome = faker.name().fullName();
		String descrizione = faker.address().streetAddress();
		Date data = (Date) faker.date().birthday();
		Sottoelemento sottoelemento = new Sottoelemento();
		sottoelemento.setData(data);
		sottoelemento.setNome(nome);
		sottoelemento.setDescrizione(descrizione);
		sottoelemento.setIdPadre(123L);

		DBServiceDao dao = new DBServiceDao();

		dao.addSottoelemento(sottoelemento);

	}

	public static void popola() {
		Faker faker = new Faker();
		String nome = faker.name().fullName();
		String descrizione = faker.address().streetAddress();
		Date data = (Date) faker.date().birthday();
		Elemento elemento = new Elemento(nome, data, descrizione);

		DBServiceDao dao = new DBServiceDao();
		dao.addElemento(elemento);
		System.out.println(elemento.getId());
		Long _id = elemento.getId();

		Double random = Math.random() * 10;
		int _random = random.intValue();
		System.out.println(_random);
		for (int i = 0; i < _random; i++) {
			String _nome = faker.name().fullName();
			String _descrizione = faker.address().streetAddress();
			Date _data = (Date) faker.date().birthday();
			Sottoelemento sottoelemento = new Sottoelemento(_nome, _data,
					_descrizione, _id);
			dao.addSottoelemento(sottoelemento);
			System.out.println(sottoelemento.getId());

		}

	}
}
