package it.db.validation;

import it.db.filter.FiltroElemento;
import it.db.model.Elemento;
import it.db.model.Sottoelemento;

public class ValidationUtil {

	public static boolean validate(Object obj) {

		if (obj instanceof Elemento)
			return validateElemento((Elemento) obj);

		if (obj instanceof Sottoelemento)
			return validateSottoelemento((Sottoelemento) obj);

		return false;
	}

	private static boolean validateElemento(Elemento elemento) {
		if (elemento.getData() == null || elemento.getNome() == null) {
			return false;

		}
		return true;
	}

	private static boolean validateSottoelemento(Sottoelemento sottoelemento) {
		if (sottoelemento.getData() == null || sottoelemento.getNome() == null
				|| sottoelemento.getIdPadre() == null) {
			return false;

		}
		return true;
	}

	public static FiltroElemento adjustFiltroElemento(
			FiltroElemento filtroElemento) {
		FiltroElemento newFiltro = new FiltroElemento();
		if (filtroElemento.getNome() != null)
			newFiltro.setNome("%" + filtroElemento.getNome() + "%");

		if (filtroElemento.getDescrizione() != null)
			newFiltro.setDescrizione("%" + filtroElemento.getDescrizione()
					+ "%");

		if (filtroElemento.getId() != null)
			newFiltro.setId(filtroElemento.getId());

		if (filtroElemento.getData() != null)
			newFiltro.setData(filtroElemento.getData());

		return newFiltro;
	}

}
