package it.db.model;

import java.io.Serializable;
import java.util.List;

public class ElementoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Elemento> listElemento;

	public List<Elemento> getListElemento() {
		return listElemento;
	}

	public void setListElemento(List<Elemento> listElemento) {
		this.listElemento = listElemento;
	}

}
