package it.db.model;

import java.io.Serializable;
import java.util.List;

public class SottoelementoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Sottoelemento> listSottoelemento;

	public List<Sottoelemento> getListSottoelemento() {
		return listSottoelemento;
	}

	public void setListSottoelemento(List<Sottoelemento> listSottoelemento) {
		this.listSottoelemento = listSottoelemento;
	}

}
