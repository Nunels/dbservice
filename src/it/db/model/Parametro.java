package it.db.model;

public class Parametro {

	private Long numero;

	public Parametro() {
	}

	public Parametro(Long numeroElementi) {
		this.numero = numeroElementi;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}
}
