package it.db.ejb;

import it.db.controller.DBServiceController;
import it.db.filter.FiltroElemento;
import it.db.model.Elemento;
import it.db.model.Sottoelemento;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@Local(DBServiceEJBLocal.class)
@LocalBean
public class DBServiceEJB implements DBServiceEJBLocal {

	DBServiceController controller = new DBServiceController();

	@Override
	public String welcomeTest() {
		return controller.welcomeTest();
	}

	@Override
	public List<Elemento> getElementi() {
		return controller.getElementi();
	}

	@Override
	public Elemento getElemento(Long id) {
		return controller.getElemento(id);
	}

	@Override
	public boolean addElemento(Elemento elemento) {
		return controller.addElemento(elemento);
	}

	@Override
	public boolean deleteElemento(Long id) {
		return controller.deleteElemento(id);
	}

	@Override
	public boolean updateElemento(Elemento elemento) {
		return controller.updateElemento(elemento);
	}

	@Override
	public List<Sottoelemento> getSottoelementiById(Long id) {
		return controller.getSottoElementiById(id);
	}

	@Override
	public List<Elemento> getElementiFilter(FiltroElemento filtroElemento) {
		return controller.getElementiFilter(filtroElemento);
	}
}
