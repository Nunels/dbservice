package it.db.ejb;

import it.db.filter.FiltroElemento;
import it.db.model.Elemento;
import it.db.model.Sottoelemento;

import java.util.List;

public interface DBServiceEJBLocal {

	public String welcomeTest();

	public Elemento getElemento(Long id);

	public List<Elemento> getElementi();

	public boolean addElemento(Elemento elemento);

	public boolean deleteElemento(Long id);

	public boolean updateElemento(Elemento elemento);

	public List<Sottoelemento> getSottoelementiById(Long id);

	public List<Elemento> getElementiFilter(FiltroElemento filtroElemento);
}
