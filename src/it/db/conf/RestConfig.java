package it.db.conf;

import io.swagger.jaxrs.listing.SwaggerSerializers;
import it.db.service.DBService;
import it.db.swagger.DBServiceApiListingResource;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class RestConfig extends Application {

	public Set<Class<?>> getClasses() {

		Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add(DBService.class);
		classes.add(DBServiceApiListingResource.class);
		classes.add(SwaggerSerializers.class);
		return classes;
	}

}
